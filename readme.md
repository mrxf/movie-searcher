# 电影资源获取工具

## 使用方法

```terminal
# 安装依赖
npm install
# 启动服务
npm start
```
在浏览器中使用：
> http://localhost:8080/movie?name=[电影名]

## 返回数据示例

```json
[
    {
        "title": "大护法",
        "link": "/btdy/dy11514.html",
        "tips": "标清",
        "cover": "http://gif-china.cc/uploads/allimg/201709/de4852ff1ead5b2f.jpg?h=190",
        "list": [
            {
                "clearity": "720p",
                "links": [
                    {
                        "title": "HD-720P 大护法.DA.HU.FA.2017.X264.AAC.国语中字.MP4[1.59GB]",
                        "magnetUrl": "magnet:?xt=urn:btih:959E2ECEB954313D38690EFF7924CA7CD80DE739"
                    }
                ]
            },
            {
                "clearity": "4k",
                "links": [
                    {
                        "title": "WEB-2160P&1080P 大护法.DA.HU.FA.2017.X264.AAC.国语中字-HQC[1.81GB]",
                        "magnetUrl": "magnet:?xt=urn:btih:6A23EC8C4CD255406E5BEF734518AA9D3B86F328"
                    }
                ]
            }
        ]
    }
]
```