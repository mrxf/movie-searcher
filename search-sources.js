const rp = require('request-promise-native');
const cheerio = require('cheerio');

class SearchSource {
    constructor() {
        this.searchUrl = `http://www.btbtdy.com/search/`;
        this.downUrl = 'http://www.btbtdy.com/vidlist/';
        this.videoCodeReg = /\d+/g;
    }

    /**
     * 获取影视详细地址
     * @param {string} movieName 影视名称
     */
    async getLinks(movieName) {
        const movieUrl = encodeURI(this.searchUrl + movieName + '.html');
        const options = {
            uri: movieUrl,
            transform: function (body) {
                return cheerio.load(body);
            }
        };
        const $ = await rp(options);
        const linkArray = [];
        $(".list_so dl dt").each((index, item) => {
            const $Links = $(item).find("a");
            const oLink = {
                "title": $Links.attr("title"),
                "link": $Links.attr("href"),
                "tips": $Links.text(),
                "cover": $Links.children("img").attr("data-src")
            }
            linkArray.push(oLink);
        })
        return linkArray;
    }

    /**
     * 获取连接数组中的下载地址
     * @param {Array} linkArray 链接数组
     */
    async getSources(linkArray) {
        const aReault = [];
        // 为防止抓取页面过多，最多只抓起3条
        const catchLength = linkArray.length > 3 ? 3: linkArray.length;
        for(let i = 0; i< catchLength; i++) {
            const oLink = linkArray[i];
            const list =  await this.getDownLinks(oLink.link);
            oLink.list = list;
            aReault.push(oLink);
        }
        return aReault;
    }

    /**
     * 根据资源地址获取详细下载地址
     * @param {string} sourceUrl 资源地址
     */
    async getDownLinks(sourceUrl) {
        const movieCode = sourceUrl.match(this.videoCodeReg)[0];
        const movieUrl = this.downUrl + movieCode + ".html";
        const urlList = [];
        const options = {
            uri: movieUrl,
            transform: function (body) {
                return cheerio.load(body);
            }
        };
        const $ = await rp(options);
        $(".p_list").each((index, item) => {
            const clearityStr = $(item).children("h2").text();
            const clearity = this.getClearTitle(clearityStr);
            const $List = $(item).children("ul");
            const sameClear = {
                "clearity": clearity,
                "links": []
            }
            /**
             * 增加下载地址判断，对于没有下载地址的标签页，跳过检索
             */
            const hasDlink = $List.find(".d1").html();
            if(!hasDlink) {
                return;
            }
            $List.children("li").each((i, e) => {
                const title = this.getCorrectTitle($(e).children("a"));
                const magnetUrl = $(e).find(".d1").attr("href");
                const oSource = {
                    "title": title,
                    "magnetUrl": $(e).find(".d1").attr("href")
                }
                sameClear.links.push(oSource);
            })
            urlList.push(sameClear);
        })
        return urlList
    }

    /**
     * 过滤字符串，获取对应的清晰度字符串
     * @param {string} clearStr 清晰度下载字符串
     */
    getClearTitle(clearStr) {
        const rC = /.*(?=下)/g;
        const matchStr = clearStr.match(rC);
        if(matchStr && matchStr.length > 0) {
            return matchStr[0];
        }
        return clearStr;
    }

    /**
     * 根据jQuery内容获取不带标签的文字标题
     * @param {jQuery} $title jQuery格式的标题 
     */
    getCorrectTitle($title) {
        return $title.contents().filter(function() {
            return this.nodeType == 3;
        }).text()
    }
}

module.exports = SearchSource;