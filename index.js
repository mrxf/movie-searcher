const restify = require("restify");
const movieHandler = require("./movie-handler");

const server = restify.createServer();
server.use(restify.plugins.queryParser());

server.get('/movie', movieHandler)

server.listen(8080, () => {
    console.log(`Sever start at ${server.name} ${server.url}`);
})