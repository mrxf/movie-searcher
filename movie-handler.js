const SearchSource = require('./search-sources');
const searchSource = new SearchSource();

/**
 * 电影路由
 * @param {*} req 
 * @param {*} res 
 */
async function movieHandler(req, res) {
    res.charSet("utf8");
    const query = Object.assign(req.query);
    if(query.hasOwnProperty("name")) {
        let linkPages = await searchSource.getLinks(query.name);
        if(linkPages.length > 0) {
            const list = await searchSource.getSources(linkPages);
            res.send(list)
        }else {
            res.send({
                "success": false,
                "errorMsg": "can't found any source"
            })
        }
    }else {
        res.send({
            "success": false,
            "errorMsg": "Without movie name"
        })
    }
}

module.exports = movieHandler;
